package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.ValidationRules;

public interface InputValidation {
    boolean validatePin(String pin);
    boolean validatePin(String pin, ValidationRules validationRule);
    boolean validatePassword(String password);
    boolean validatePassword(String password, ValidationRules validationRule);
    boolean validateMandatory(String inputText);
    boolean validateEmail(String email);
    boolean validatePhone(String phone);
    boolean validatePhone(String phone, ValidationRules validationRule);
    boolean validateDate(String date);
    boolean validateDate(String date, ValidationRules validationRule);
    boolean validateDateTime(String dateTime);
    boolean validateDateTime(String dateTime, ValidationRules validationRule);
    boolean validateField(String text, ValidationTypes validationType);
    ValidationError.ErrorEnums getOccurredError();
    void setOccurredError(ValidationError.ErrorEnums error);

    enum ValidationTypes {
        TYPE_PIN,
        TYPE_PASSWORD,
        TYPE_MANDATORY,
        TYPE_EMAIL,
        TYPE_DATE,
        TYPE_DATE_TIME,
        TYPE_PHONE;
    }
}
