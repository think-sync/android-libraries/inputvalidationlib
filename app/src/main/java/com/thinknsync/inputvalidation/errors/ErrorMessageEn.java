package com.thinknsync.inputvalidation.errors;

public class ErrorMessageEn implements ErrorMessageStrings {

    @Override
    public String getLanguageCode() {
        return LanguageCodes.LANGUAGE_CODE_ENGLISH;
    }

    @Override
    public String getNotEmailMessage() {
        return "Email address invalid";
    }

    @Override
    public String getInvalidCharacterCountMessage() {
        return "Must be at least %d digits";
    }

    @Override
    public String getMandatoryFieldMessage() {
        return "Field must not be empty";
    }

    @Override
    public String getNotNumericMessage() {
        return "Field must be Numeric only";
    }

    @Override
    public String getInvalidPasswordMessage() {
        return "Field must contain capital, small, numeric and special characters";
    }

    @Override
    public String getNumericSequenceMessage() {
        return "Cannot be sequence number";
    }

    @Override
    public String getNumericReverseSequenceMessage() {
        return "Cannot be a reverse sequence number";
    }

    @Override
    public String getHasPatternMessage() {
        return "Cannot have numeric pattern";
    }

    @Override
    public String getStartsWith0Message() {
        return "Cannot start with 0";
    }

    @Override
    public String getNumericRepetitionMessage() {
        return "Cannot be repeated number";
    }

    @Override
    public String getInvalidPhoneMessage() {
        return "Phone number invalid";
    }

    @Override
    public String getInvalidDateMessage() {
        return "Invalid date";
    }

    @Override
    public String getInvalidDateTimeMessage() {
        return "Invalid date time ";
    }

    @Override
    public String getUnknownErrorMessage() {
        return "Unknown error";
    }

    @Override
    public String getNoErrorMessage() {
        return "No error occurred";
    }

    @Override
    public String getTextMismatchErrorMessage() {
        return "Fields do not match";
    }
}