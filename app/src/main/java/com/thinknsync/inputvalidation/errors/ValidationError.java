package com.thinknsync.inputvalidation.errors;

import com.thinknsync.inputvalidation.validationRules.PasswordValidationRules;
import com.thinknsync.inputvalidation.validationRules.PinValidationRules;

public class ValidationError {

    private static ErrorMessageStrings errorMessageStrings = new ErrorMessageEn();

    public void setErrorMessageStrings(ErrorMessageStrings errorMessageStrings) {
        ValidationError.errorMessageStrings = errorMessageStrings;
    }

    public enum ErrorEnums {
        NOT_EMAIL(401, errorMessageStrings.getNotEmailMessage()),
        INVALID_CHARACTER_COUNT_PIN(402, String.format(
                errorMessageStrings.getInvalidCharacterCountMessage(), PinValidationRules.PIN_MIN_LENGTH)),
        INVALID_CHARACTER_COUNT_PASSWORD(412, String.format(
                errorMessageStrings.getInvalidCharacterCountMessage(), PasswordValidationRules.PASSWORD_MIN_LENGTH)),
        EMPTY(403, errorMessageStrings.getMandatoryFieldMessage()),
        NOT_NUMERIC(404, errorMessageStrings.getNotNumericMessage()),
        INVALID_PASSWORD(414, errorMessageStrings.getInvalidPasswordMessage()),
        NUMERIC_SEQUENCE(405, errorMessageStrings.getNumericSequenceMessage()),
        NUMERIC_REVERSE_SEQUENCE(406, errorMessageStrings.getNumericReverseSequenceMessage()),
        HAS_PATTERN(407, errorMessageStrings.getHasPatternMessage()),
        STARTS_WITH_0(409, errorMessageStrings.getStartsWith0Message()),
        NUMERIC_REPETITION(410, errorMessageStrings.getNumericRepetitionMessage()),
        INVALID_PHONE_NUMBER(411, errorMessageStrings.getInvalidPhoneMessage()),
        TEXTS_DONT_MATCH(408, errorMessageStrings.getTextMismatchErrorMessage()),
        NOT_DATE(415, errorMessageStrings.getInvalidDateMessage()),
        NOT_DATE_TIME(416, errorMessageStrings.getInvalidDateTimeMessage()),

        UNKNOWN(500, errorMessageStrings.getUnknownErrorMessage()),
        NO_ERROR(400, errorMessageStrings.getNoErrorMessage());

        private int id;
        private String errorText;

        ErrorEnums( int id, String errorText){
            this.id = id;
            this.errorText = errorText;
        }

        public int getId () {
            return id;
        }

        public String getErrorText () {
            return errorText;
        }

        public ErrorEnums getErrorById ( int id){
            for (ErrorEnums e : values()) {
                if (e.getId() == id) {
                    return e;
                }
            }
            return UNKNOWN;
        }

        public ErrorEnums getErrorByText (String text){
            for (ErrorEnums e : values()) {
                if (e.getErrorText().equals(text)) {
                    return e;
                }
            }
            return UNKNOWN;
        }
    }
}
