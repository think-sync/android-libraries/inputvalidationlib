package com.thinknsync.inputvalidation.errors;

public interface ErrorMessageStrings {
    String getLanguageCode();
    String getNotEmailMessage();
    String getInvalidCharacterCountMessage();
    String getMandatoryFieldMessage();
    String getNotNumericMessage();
    String getInvalidPasswordMessage();
    String getNumericSequenceMessage();
    String getNumericReverseSequenceMessage();
    String getHasPatternMessage();
    String getStartsWith0Message();
    String getNumericRepetitionMessage();
    String getInvalidPhoneMessage();
    String getInvalidDateMessage();
    String getInvalidDateTimeMessage();
    String getUnknownErrorMessage();
    String getNoErrorMessage();
    String getTextMismatchErrorMessage();
}
