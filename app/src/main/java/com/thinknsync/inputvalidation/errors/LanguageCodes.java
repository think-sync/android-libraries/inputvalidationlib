package com.thinknsync.inputvalidation.errors;

public interface LanguageCodes {
    String LANGUAGE_CODE_ENGLISH = "en";
    String LANGUAGE_CODE_BANGLA = "bn";
}
