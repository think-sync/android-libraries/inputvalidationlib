package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.DateTimeValidation;
import com.thinknsync.inputvalidation.validationRules.DateValidation;
import com.thinknsync.inputvalidation.validationRules.EmailValidationRule;
import com.thinknsync.inputvalidation.validationRules.MandatoryFieldValidation;
import com.thinknsync.inputvalidation.validationRules.PasswordValidationRules;
import com.thinknsync.inputvalidation.validationRules.PhoneValidationBd;
import com.thinknsync.inputvalidation.validationRules.PinValidationRules;
import com.thinknsync.inputvalidation.validationRules.ValidationRules;


public class ValidateField implements InputValidation {

    private ValidationError.ErrorEnums error;

    @Override
    public boolean validatePin(String pin){
        return validatePin(pin, new PinValidationRules());
    }

    @Override
    public boolean validatePin(String pin, ValidationRules validationRule){
        return validateField(pin, validationRule);
    }

    @Override
    public boolean validatePassword(String password) {
        return validatePassword(password, new PasswordValidationRules());
    }

    @Override
    public boolean validatePassword(String password, ValidationRules validationRule) {
        return validateField(password, validationRule);
    }

    private boolean validateField(String field, ValidationRules validationRule) {
        if(!validationRule.validate(field)){
            setOccurredError(validationRule.getValidationError());
            return false;
        }
        return true;
    }

    @Override
    public boolean validateMandatory(String inputText) {
        return validateField(inputText, new MandatoryFieldValidation());
    }

    @Override
    public boolean validateEmail(String email) {
        return validateField(email, new EmailValidationRule());
    }

    @Override
    public boolean validatePhone(String phone) {
        return validatePhone(phone, new PhoneValidationBd());
    }

    @Override
    public boolean validatePhone(String phone, ValidationRules validationRule) {
        return validateField(phone, validationRule);
    }

    @Override
    public boolean validateDate(String date) {
        return validateDate(date, new DateValidation());
    }

    @Override
    public boolean validateDate(String date, ValidationRules validationRule) {
        return validateField(date, validationRule);
    }

    @Override
    public boolean validateDateTime(String dateTime) {
        return validateDateTime(dateTime, new DateTimeValidation());
    }

    @Override
    public boolean validateDateTime(String dateTime, ValidationRules validationRule) {
        return validateField(dateTime, validationRule);
    }

    @Override
    public boolean validateField(String text, ValidationTypes validationType) {
        switch (validationType) {
            case TYPE_PIN:
                return validatePin(text);
            case TYPE_PASSWORD:
                return validatePassword(text);
            case TYPE_MANDATORY:
                return validateMandatory(text);
            case TYPE_EMAIL:
                return validateEmail(text);
            case TYPE_PHONE:
                return validatePhone(text);
            case TYPE_DATE:
                return validateDate(text);
            case TYPE_DATE_TIME:
                return validateDateTime(text);
            default:
                this.error = getOccurredError();
                return false;
        }
    }

    @Override
    public ValidationError.ErrorEnums getOccurredError() {
        return error;
    }


    @Override
    public void setOccurredError(ValidationError.ErrorEnums error) {
        this.error = error;
    }
}
