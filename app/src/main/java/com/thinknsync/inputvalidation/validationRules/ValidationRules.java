package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public interface ValidationRules {
    boolean validate(String input);
    ValidationError.ErrorEnums getValidationError();
}
