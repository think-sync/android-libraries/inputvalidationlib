package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public class PasswordValidationRules extends BaseValidationRule {

    public static final int PASSWORD_MIN_LENGTH = 6;

    @Override
    public boolean validate(String input) {
        return hasMinCharacterCount(input) && isAlphaNumericSpecialCapAndSmall(input);
    }

    private boolean hasMinCharacterCount(String input) {
        boolean matches = input.length() >= PASSWORD_MIN_LENGTH;
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PASSWORD);
        return matches;
    }

    private boolean isAlphaNumericSpecialCapAndSmall(String input) {
        boolean matches = input.matches(ValidationRegEx.ALPHA_NUMERIC_CAP_SMALL_SPECIAL_REG_EX);
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.INVALID_PASSWORD);
        return matches;
    }
}
