package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public class EmailValidationRule extends BaseValidationRule {
    @Override
    public boolean validate(String input) {
        boolean matches = input.matches(ValidationRegEx.EMAIL_REG_EX);
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.NOT_EMAIL);
        return matches;
    }
}
