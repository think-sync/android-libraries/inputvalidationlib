package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeValidation extends BaseValidationRule {

    @Override
    public boolean validate(String input) {
        return isValidDateTimeFormat(input, "yyyy-MM-dd HH:mm:ss")
                || isValidDateTimeFormat(input, "yyyy-MM-dd HH:mm")
                || isValidDateTimeFormat(input, "yyyy-MM-dd hh:mm aa")
                || isValidDateTimeFormatForText(input);
    }

    private boolean isValidDateTimeFormat(String input, String dateFormat){
//        String[] dateSegments = input.split(" ");
//        if(dateSegments.length < 2){
//            return setErrorAndReturn();
//        }
//        DateValidation dateValidation = new DateValidation();
//        if(dateValidation.validate(dateSegments[0])){
//            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat.split(" ")[1]);
//            sdf.setLenient(false);
//            boolean matchFormat = sdf.parse(input, new ParsePosition(0)) != null;
//            setErrorIfApplicable(matchFormat, ValidationError.ErrorEnums.NOT_DATE_TIME);
//            return matchFormat;
//        } else {
//            return setErrorAndReturn();
//        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        boolean matchFormat = sdf.parse(input, new ParsePosition(0)) != null;
        setErrorIfApplicable(matchFormat, ValidationError.ErrorEnums.NOT_DATE_TIME);
        return matchFormat;
    }

    private boolean isValidDateTimeFormatForText(String input){
        String[] dateSegments = input.replace(",", "").replace("th", "")
                .split(" ");
        if(dateSegments.length < 3){
            return setErrorAndReturn();
        }
        try {
            Date date = new SimpleDateFormat("MMMM").parse(dateSegments[1]);
            Calendar cal = Calendar.getInstance();
            cal.setLenient(false);
            cal.setTime(date);
            cal.set(Integer.valueOf(dateSegments[2]), cal.get(Calendar.MONTH), Integer.valueOf(dateSegments[0]));
            return true;
        } catch (Exception e) {
            return setErrorAndReturn();
        }
    }

    private boolean setErrorAndReturn(){
        setErrorIfApplicable(false, ValidationError.ErrorEnums.NOT_DATE_TIME);
        return false;
    }
}
