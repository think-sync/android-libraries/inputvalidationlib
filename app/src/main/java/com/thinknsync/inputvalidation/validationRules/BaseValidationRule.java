package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public abstract class BaseValidationRule implements ValidationRules {

    protected ValidationError.ErrorEnums error = ValidationError.ErrorEnums.NO_ERROR;

    @Override
    public ValidationError.ErrorEnums getValidationError() {
        return error;
    }


    protected void setErrorIfApplicable(boolean result, ValidationError.ErrorEnums setError){
        if(!result) {
            error = setError;
        }
    }
}
