package com.thinknsync.inputvalidation.validationRules;

public interface ValidationRegEx {
    String ALPHA_NUMERIC_CAP_SMALL_SPECIAL_REG_EX = "^(?=.*?\\p{Lu})(?=.*?\\p{Ll})(?=.*?\\d)" +
            "(?=.*?[`~!@#$%^&*()\\-_=+\\\\|\\[{\\]};:'\",<.>/?]).*$";
    String NUMERIC_REG_EX = "[0-9]+";
    String PATTERN_REG_EX = "^(.+)(?:\\1)+$";
    String EMAIL_REG_EX = "^.+@.+\\..+$";
}
