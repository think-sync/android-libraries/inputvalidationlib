package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public class PhoneValidationBd extends BaseValidationRule {

    public static final int PHONE_NUMBER_DIGIT_COUNT = 14;

    @Override
    public boolean validate(String input) {
        if(input.startsWith("0")){
            input = "+88" + input;
        } else if(input.startsWith("1")){
            input = "+880" + input;
        }

        return matchesStartingCharacters(input) && matchesDigitCount(input);
    }

    private boolean matchesStartingCharacters(String input) {
        boolean matches = input.startsWith("+8801");
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
        return matches;
    }

    private boolean matchesDigitCount(String input) {
        boolean matches = input.length() == PHONE_NUMBER_DIGIT_COUNT;
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
        return matches;
    }
}
