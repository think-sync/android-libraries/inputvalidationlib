package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class PinValidationRules extends BaseValidationRule {

    public static final int PIN_MIN_LENGTH = 6;

    @Override
    public boolean validate(String input) {
        return hasOnlyNumbers(input) && hasMinCharacterCount(input) && !hasPattern(input) &&
                !startsWith0(input) && !hasNumericSequence(input) && !hasNumericReverseSequence(input);
    }

    private boolean hasOnlyNumbers(String input) {
        boolean matches = input.matches(ValidationRegEx.NUMERIC_REG_EX);
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.NOT_NUMERIC);
        return matches;
    }

    private boolean hasMinCharacterCount(String input) {
        boolean matches = input.length() >= PIN_MIN_LENGTH;
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PIN);
        return matches;
    }

    private boolean hasPattern(String input) {
        boolean matches = input.matches(ValidationRegEx.PATTERN_REG_EX);
        setErrorIfApplicable(!matches, ValidationError.ErrorEnums.HAS_PATTERN);
        return matches;
    }

//    private boolean hasNumericRepetition(String input) {
//        List<Character> uniqueStringsArray = new ArrayList<>(new HashSet<>(getCharacterListFromString(input)));
//        boolean matches = uniqueStringsArray.size() == 1;
//        setErrorIfApplicable(matches, ValidationError.NUMERIC_REPETITION);
//        return matches;
//    }

//    private List<Character> getCharacterListFromString(String input){
//        List<Character> charList = new ArrayList<>();
//        for (char c : input.toCharArray()) {
//            charList.add(c);
//        }
//        return charList;
//    }

    private boolean hasNumericSequence(String input) {
        List<Boolean> results = new ArrayList<>();
        for (int i = 0; i < input.length() - 1; i++) {
            results.add((int) input.charAt(i + 1) - (int) input.charAt(i) == 1);
        }

        boolean matches = !results.contains(false);
        setErrorIfApplicable(!matches, ValidationError.ErrorEnums.NUMERIC_SEQUENCE);
        return matches;
    }

    private boolean startsWith0(String input) {
        boolean matches = input.startsWith("0");
        setErrorIfApplicable(!matches, ValidationError.ErrorEnums.STARTS_WITH_0);
        return matches;
    }

    private boolean hasNumericReverseSequence(String input) {
        boolean matches = hasNumericSequence(new StringBuilder(input).reverse().toString());
        setErrorIfApplicable(!matches, ValidationError.ErrorEnums.NUMERIC_REVERSE_SEQUENCE);
        return matches;
    }
}
