package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

public class MandatoryFieldValidation extends BaseValidationRule {
    @Override
    public boolean validate(String input) {
        boolean matches = input.length() > 0;
        setErrorIfApplicable(matches, ValidationError.ErrorEnums.EMPTY);
        return matches;
    }
}
