package com.thinknsync.inputvalidation.validationRules;

import com.thinknsync.inputvalidation.errors.ValidationError;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateValidation extends BaseValidationRule {

    @Override
    public boolean validate(String input) {
        return isValidDateFormat(input, "yyyy-MM-dd")
                || isValidDateFormat(input, "dd-MM-yyyy")
                || isValidDateFormat(input, "MM-dd-yyyy")
                || isValidDateFormat(input, "yyyy/MM/dd")
                || isValidDateFormat(input, "dd/MM/yyyy")
                || isValidDateFormat(input, "MM/dd/yyyy")
                || isValidDateTimeFormatForText(input);
    }

    private boolean isValidDateFormat(String input, String dateFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        boolean matchFormat = sdf.parse(input, new ParsePosition(0)) != null;
        setErrorIfApplicable(matchFormat, ValidationError.ErrorEnums.NOT_DATE);
        return matchFormat;
    }

    private boolean isValidDateTimeFormatForText(String input){
        String[] dateSegments = input.replace(",", " ").replace("th", "")
                .replace("  ", " ").split(" ");
        if(dateSegments.length < 3){
            return setErrorAndReturn();
        }
        try {
            Date date = new SimpleDateFormat("MMMM").parse(dateSegments[1]);
            Calendar cal = Calendar.getInstance();
            cal.setLenient(false);
            cal.setTime(date);
//            System.out.println(cal.get(Calendar.MONTH));
            cal.set(Integer.valueOf(dateSegments[2]), cal.get(Calendar.MONTH), Integer.valueOf(dateSegments[0]));
            return true;
        } catch (Exception e) {
            return setErrorAndReturn();
        }
    }

    private boolean setErrorAndReturn(){
        setErrorIfApplicable(false, ValidationError.ErrorEnums.NOT_DATE);
        return false;
    }
}
