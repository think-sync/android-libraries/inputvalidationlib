package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.PasswordValidationRules;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class PasswordValidatorTests {

    private PasswordValidationRules passwordValidationRule = new PasswordValidationRules();

    @Test
    public void validatePassword(){
        assertFalse(passwordValidationRule.validate(""));
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PASSWORD);

        assertFalse(passwordValidationRule.validate("abcde"));
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PASSWORD);

        assertFalse(passwordValidationRule.validate("abc12"));
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PASSWORD);

        assertFalse(passwordValidationRule.validate("abcdefgh"));  //only small
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("ABCDEFGH"));//only cap
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("abcABC"));//only cap and small
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("ABCDEFGH12"));//only cap, small and numeric
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("aBc!@!^"));//only cap, small and special
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("abc!@2"));//only small, numeric and special
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertFalse(passwordValidationRule.validate("ABC!@2"));//only cap, numeric and special
        assertEquals(passwordValidationRule.getValidationError(), ValidationError.ErrorEnums.INVALID_PASSWORD);

        assertTrue(passwordValidationRule.validate("aBc!@3"));
        assertTrue(passwordValidationRule.validate("+#aBc!@3"));
        assertTrue(passwordValidationRule.validate("acDb@#@345"));
    }

}
