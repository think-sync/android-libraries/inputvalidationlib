package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.EmailValidationRule;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class EmailValidatorTests {

    private EmailValidationRule emailValidation = new EmailValidationRule();

    @Test
    public void blankStringReturnsInvalidEmailError(){
        assertFalse(emailValidation.validate(""));
        assertEquals(emailValidation.getValidationError(), ValidationError.ErrorEnums.NOT_EMAIL);
    }

    @Test
    public void numberReturnsInvalidEmailError(){
        assertFalse(emailValidation.validate("2374"));
        assertEquals(emailValidation.getValidationError(), ValidationError.ErrorEnums.NOT_EMAIL);
    }

    @Test
    public void missingDotReturnsInvalidEmailError(){
        assertFalse(emailValidation.validate("a@bc"));
        assertEquals(emailValidation.getValidationError(), ValidationError.ErrorEnums.NOT_EMAIL);
    }

    @Test
    public void validEmailReturnsInvalidEmailError(){
        assertTrue(emailValidation.validate("a@bc.c"));
    }
}
