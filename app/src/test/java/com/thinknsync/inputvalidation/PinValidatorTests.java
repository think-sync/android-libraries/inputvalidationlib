package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.PinValidationRules;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertFalse;

public class PinValidatorTests {

    private PinValidationRules pinValidationRules = new PinValidationRules();

    @Test
    public void validatePin(){
        assertFalse(pinValidationRules.validate(""));       //blank
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.NOT_NUMERIC);

        assertFalse(pinValidationRules.validate("2374"));  //char count 5
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.INVALID_CHARACTER_COUNT_PIN);

        assertFalse(pinValidationRules.validate("abc123")); //alpha numeric
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.NOT_NUMERIC);

        assertFalse(pinValidationRules.validate("025753")); //starts with 0
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.STARTS_WITH_0);

        assertFalse(pinValidationRules.validate("121212")); //pattern (2 digits)
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.HAS_PATTERN);

        assertFalse(pinValidationRules.validate("123123")); //pattern (3 digits)
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.HAS_PATTERN);

        assertFalse(pinValidationRules.validate("123456")); //sequence
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.NUMERIC_SEQUENCE);

        assertFalse(pinValidationRules.validate("654321")); //inverse sequence
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.NUMERIC_REVERSE_SEQUENCE);

        assertFalse(pinValidationRules.validate("666666")); //repetition
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.HAS_PATTERN);

        assertFalse(pinValidationRules.validate("-25852")); //negative
        assertEquals(pinValidationRules.getValidationError(), ValidationError.ErrorEnums.NOT_NUMERIC);

        assertTrue(pinValidationRules.validate("258520"));  //correct pin
        assertTrue(pinValidationRules.validate("2274521"));//valid char count 7
    }
}
