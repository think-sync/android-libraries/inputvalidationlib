package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.DateValidation;
import com.thinknsync.inputvalidation.validationRules.PasswordValidationRules;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class DateValidatorTests {

    private DateValidation dateValidationRule = new DateValidation();

    @Test
    public void validateDate(){
        assertFalse(dateValidationRule.validate(""));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("abcde"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("2020-22-22"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("2020-12-32"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("2020-13-22"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("2020-22-22 22:10:10"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertFalse(dateValidationRule.validate("30 juee 2020"));
        assertEquals(dateValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE);

        assertTrue(dateValidationRule.validate("2020-12-22"));
        assertTrue(dateValidationRule.validate("2020-5-5"));
        assertTrue(dateValidationRule.validate("2020-05-05"));
        assertTrue(dateValidationRule.validate("22-12-2020"));
        assertTrue(dateValidationRule.validate("12-22-2020"));
        assertTrue(dateValidationRule.validate("2020/12/22"));
        assertTrue(dateValidationRule.validate("22/12/2020"));
        assertTrue(dateValidationRule.validate("12/22/2020"));
        assertTrue(dateValidationRule.validate("30 June 2020"));
        assertTrue(dateValidationRule.validate("30th June 2020"));
        assertTrue(dateValidationRule.validate("30th June,2020"));
        assertTrue(dateValidationRule.validate("30th June, 2020"));
        assertTrue(dateValidationRule.validate("30 June, 2020"));
        assertTrue(dateValidationRule.validate("30 sep, 2020"));
        assertTrue(dateValidationRule.validate("30 Sep, 2020"));
    }

}
