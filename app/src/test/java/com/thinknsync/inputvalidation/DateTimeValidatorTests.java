package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.DateTimeValidation;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class DateTimeValidatorTests {

    private DateTimeValidation dateTimeValidationRule = new DateTimeValidation();

    @Test
    public void validateDate(){
        assertFalse(dateTimeValidationRule.validate(""));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("abcde"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-22-22"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-12-32"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-13-22"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-22-22 22:10:10"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-11-22"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-11-11"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("22-12-2020 22:10:10"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-14-12 22:10:10"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-12-22 25:10:10"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertFalse(dateTimeValidationRule.validate("2020-12-22 22:62:10"));
        assertEquals(dateTimeValidationRule.getValidationError(), ValidationError.ErrorEnums.NOT_DATE_TIME);

        assertTrue(dateTimeValidationRule.validate("2020-12-22 22:10:10"));
        assertTrue(dateTimeValidationRule.validate("2020-5-5 22:10:10"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 22:10"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 10:10 pm"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 10:10 AM"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 10:10:20 AM"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 10:10:20.120"));
        assertTrue(dateTimeValidationRule.validate("2020-12-22 10:10:20:120"));
    }

}
