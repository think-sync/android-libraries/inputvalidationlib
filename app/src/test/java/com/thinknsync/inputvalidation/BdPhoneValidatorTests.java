package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.PhoneValidationBd;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class BdPhoneValidatorTests {

    private PhoneValidationBd phoneValidation = new PhoneValidationBd();

    @Test
    public void emptyNumberReturnsInvalidPhoneError() {
        assertFalse(phoneValidation.validate(""));
        assertEquals(phoneValidation.getValidationError(), ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
    }

    @Test
    public void randomNumberReturnsInvalidPhoneError(){
        assertFalse(phoneValidation.validate("2374"));
        assertEquals(phoneValidation.getValidationError(), ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
    }

    @Test
    public void tenDigitNumberReturnsInvalidPhoneError(){
        assertFalse(phoneValidation.validate("0176534526"));
        assertEquals(phoneValidation.getValidationError(), ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
    }

   @Test
    public void unknownCountryCodeNumberReturnsInvalidPhoneError(){
       assertFalse(phoneValidation.validate("+80176534526"));
       assertEquals(phoneValidation.getValidationError(), ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
    }

    @Test
    public void startsWith88NumberReturnsInvalidPhoneError(){
        assertFalse(phoneValidation.validate("+8817653452622"));
        assertEquals(phoneValidation.getValidationError(), ValidationError.ErrorEnums.INVALID_PHONE_NUMBER);
    }

    @Test
    public void startsWith880NumberReturnsValidPhone(){
        assertTrue(phoneValidation.validate("+8801765345262"));
    }

    @Test
    public void validPhoneWithoutCountryCodeReturnsValidPhone(){
        assertTrue(phoneValidation.validate("01761735678"));
    }

    @Test
    public void validPhonestartingWih1ReturnsValidPhone(){
        assertTrue(phoneValidation.validate("1765345263"));
    }
}
