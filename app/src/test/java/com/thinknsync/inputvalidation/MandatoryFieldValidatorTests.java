package com.thinknsync.inputvalidation;

import com.thinknsync.inputvalidation.errors.ValidationError;
import com.thinknsync.inputvalidation.validationRules.EmailValidationRule;
import com.thinknsync.inputvalidation.validationRules.MandatoryFieldValidation;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MandatoryFieldValidatorTests {

    private MandatoryFieldValidation mandatoryValidation = new MandatoryFieldValidation();

    @Test
    public void blankStringReturnsInvalidEmailError(){
        assertFalse(mandatoryValidation.validate(""));
        assertEquals(mandatoryValidation.getValidationError(), ValidationError.ErrorEnums.EMPTY);
    }

    @Test
    public void anyCharacterStringReturnsValidValue(){
        assertTrue(mandatoryValidation.validate("a"));
    }
}
